# Repository History

For full details, see the Git logs.  

## Sunday, June 27<sup>th</sup>, 2021

 - Repository renamed from 'Writing and Story Treasury and Idea Archive' to 'Creative Sandbox.'  

## Wednesday, August 5<sup>th</sup>, 2020

 - HISTORY.md:  
   - Inverted this change log's entries' sort order from chronological to reverse chronological&thinsp;—&thinsp;to put its newest entries at the top, naturally.  
 - README.md:  
   - Added an explicit note that this repository is laid out in [my 'Semantic Looseleaf Workspace' format](https://gitlab.com/RandomDSdevel/semantic-looseleaf-workspace-specification-and-tooling).  

## Tuesday, March 24<sup>th</sup>, 2020

 - Repository renamed from 'Writing Warren' to 'Writing and Story Treasury and Idea Archive.'  

## Wednesday, October 2<sup>nd</sup>, 2019

 - Initial creation.  (🎉!)  