# RandomDSdevel's/WhenCatsFoxesandWolvesFly's Creative Sandbox

---

_Note:_  This repository is laid out in [my 'Semantic Looseleaf Workspace' format](https://gitlab.com/RandomDSdevel/semantic-looseleaf-workspace-specification-and-tooling).  

---

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I've collected some story ideas of mine here, though that's all I've done so far.  This repository doesn't have an official license yet, but consider everything hosted herein to be free for anyone to ninja, borrow, steal, reuse, remix, and/or otherwise draw inspiration from.  (I have a heavy preference for attribution in any case, though.)  
